function upperCaseFirst(str) {
    // str ← строка которая в нашем случае равна 'pitter' или ''
    // РЕШЕНИЕ НАЧАЛО
    let result;
    if (str.length <= 1){
        result =  str.toUpperCase(); // toUpperCase на тот случай если не пустая строка, а один символ
    } else{
        result = str[0].toUpperCase() + str.slice(1);
    }
    // РЕШЕНИЕ КОНЕЦ

    return result;
}
// console.log(upperCaseFirst('pitter')); // Pitter
// console.log(upperCaseFirst('')); // ''
upperCaseFirst('pitter');
upperCaseFirst('');