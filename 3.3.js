function truncate(string, maxLength) {
    // РЕШЕНИЕ НАЧАЛО
    if (string.length > maxLength) {
        return string.slice(0, maxLength - 3) + '...';
    } else {
        return string;
    }
    // РЕШЕНИЕ КОНЕЦ
}
console.log(truncate('Вот, что мне хотелось бы сказать на эту тему:', 21)); // 'Вот, что мне хотел...'