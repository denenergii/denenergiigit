const PRICE = '$120';
function extractCurrencyValue(source) {
//     // РЕШЕНИЕ НАЧАЛО
    return (source.length > 1) ? parseInt(source.slice(1)) : null;
//     // РЕШЕНИЕ КОНЕЦ
}   
console.log(extractCurrencyValue(PRICE)); // 120
console.log(typeof extractCurrencyValue(PRICE)); // number
console.log(extractCurrencyValue({})); // null