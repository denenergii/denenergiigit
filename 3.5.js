const person = {
    get galary () {
        return `Зарплата за проект составляет ${this.rate * this.hours}$`;},
}
Object.defineProperty (person, 'rate', {
    value: '20',
    configurable: false,
});
Object.defineProperty (person, 'hours', {
    value: '5',
    configurable: false,
});
Object.freeze(person);
console.log (person.galary);