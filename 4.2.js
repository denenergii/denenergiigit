const array = [1, 2, 3, 4, 5];
const INITIAL_ACCUMULATOR = 6;

// Решение
function reduce(array, func) {
    let sum = 0;

    for (let i = 0; i < array.length; i++) {
        const element = array[i];
        const isValid = func(element, i, array);
        
        if(typeof array[i] != "number"){
            return(typeof array[i])
        }
        else if(isValid) {
            sum = sum + array[i];
        }
    }
    return sum + INITIAL_ACCUMULATOR;
}

const result = reduce(array,function (accumulator, element, index, arrayRef) {
        // console.log(`${index}:`, accumulator, element, arrayRef);
        
        return accumulator + element;
    },
    INITIAL_ACCUMULATOR,
);

console.log(result); // 21