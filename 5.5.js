  function createFibonacciGenerator() {
    let a = 1;
    let b = 1;
    let c = 0;
    return {
      print: function getNext() {
        [c, a, b] = [a, b, a + b];
        return c;
    },
    reset: function getNext() {
      [c, a, b] = [0, 1, 1];
    }};
}
// document.myForm.reset();
const generator1 = createFibonacciGenerator();

console.log(generator1.print()); // 1
console.log(generator1.print()); // 1
console.log(generator1.print()); // 2
console.log(generator1.print()); // 3
console.log(generator1.reset()); // undefined
console.log(generator1.print()); // 1
console.log(generator1.print()); // 1
console.log(generator1.print()); // 2

const generator2 = createFibonacciGenerator();

console.log(generator2.print()); // 1
console.log(generator2.print()); // 1
console.log(generator2.print()); // 2

// exports.createFibonacciGenerator = createFibonacciGenerator;