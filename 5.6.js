// function createLogger(){
//     return{
//         call: function (name, a, b){
//             let res;
//             if (name == sum){
//                 console.log(res);
//                 return (res = a + b)}
//             else{
//                 console.log(res);
//                 return a;
//             }
//         },
//         print: function (name, num, res){
//             return `name: ${name}, in: ${num}, res: ${res}`;
//         }
//     }
// }

function createLogger() {
    let res = [];
    function call(callback, ...funcArg) {
      if (typeof callback !== 'function') {
        throw new TypeError('callback should be a function!');
       }
      res.push({
         name: callback.name,
         in: [...funcArg],
         out: callback(...funcArg),
       });
       return res[res.length-1].out;
     }
     function print(){return res}
     return { call, print }
   }

const returnIdentity = n => n;
const sum = (a, b) => a + b;
const returnNothing = () => {};

const logger1 = createLogger();
console.log(logger1.call(returnIdentity, 1)); // 1
console.log(logger1.call(sum, 1, 2)); // 3
console.log(logger1.print()); // [ { name: 'returnIdentity', in: [ 1 ], out: 1 }, { name: 'sum', in: [ 1, 2 ], out: 3 } ]

const logger2 = createLogger();
console.log(logger2.call(sum, 3, 4)); // 7
console.log(logger2.call(returnIdentity, 9)); // 9
console.log(logger2.call(returnNothing)); // undefined
console.log(logger2.print()); // [ { name: 'sum', in: [ 3, 4 ], out: 7 }, { name: 'returnIdentity', in: [ 9 ], out: 9 }, { name: 'returnNothing', in: [], out: undefined } ]