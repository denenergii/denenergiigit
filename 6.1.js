function shallowMerge(key1, key2){
    const userInfo = Object.assign(
        {},
        key1,
        key2,
    )
    Object.defineProperty(userInfo, 'firstName', { writable:false }) ;
    Object.defineProperty(userInfo, 'job', { configurable: false }) ;
    return(userInfo);
}


const user = { firstName: 'Marcus', lastName: 'Kronenberg' };
const userData = { job: 'developer', country: 'Germany', lastName: 'Schmidt' };

Object.defineProperty(user, 'firstName', { writable: false });
Object.defineProperty(userData, 'job', { configurable: false });

const result = shallowMerge(user, userData);

console.log(result); // { firstName: 'Marcus', lastName: 'Schmidt', job: 'developer', country: 'Germany' }
console.log(Object.getOwnPropertyDescriptor(result, 'firstName').writable); // false
console.log(Object.getOwnPropertyDescriptor(result, 'job').configurable); // false