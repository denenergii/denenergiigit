function calculateAdvanced(...funcArg){
    let res = [];
    let prevResult = 0;
        for(let val of funcArg){
            try{
                const result = val(prevResult);
                if (result === undefined){
                    throw new Error('callback at index 0 did not return any value.');
                }else{
                    prevResult = val(prevResult)
                }
                res.push({
                value: prevResult,
                  });
            }catch(e){
                res.push({
                    errors: 
                        {index: funcArg.indexOf(val),
                        name:e.name,
                        message:e.message,}                
                  });
            }
        }return(res)
}  
    const result = calculateAdvanced(
        () => {},
        () => {
            return 7;
        },
        () => {},
        prevResult => {
            return prevResult + 4;
        },
        () => {
            throw new RangeError('Range is too big.');
        },
        prevResult => {
            return prevResult + 1;
        },
        () => {
            throw new ReferenceError('ID is not defined.');
        },
        prevResult => {
            return prevResult * 5;
        },
    );
    console.log(result)

// Функция вернёт:
// { value: 60,
//     errors:
//      [ { index: 0,
//          name: 'Error',
//          message: 'callback at index 0 did not return any value.' },
//        { index: 2,
//          name: 'Error',
//          message: 'callback at index 2 did not return any value.' },
//        { index: 4, name: 'RangeError', message: 'Range is too big.' },
//        { index: 6,
//          name: 'ReferenceError',
//          message: 'ID is not defined.' } ] }