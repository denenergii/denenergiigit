const formatDate = (date, format, delimiter) => {
    let y =  `${date.getFullYear()}`;
    let m = `${date.getMonth()}`;
    let d = `${date.getDate()}`
    if(delimiter === undefined){
        delimiter = '.'    }
    if (!(format === 'DD MM YYYY' || format === 'YYYY' || format === 'DD MM')){
        throw new TypeError('callback should be a DD MM YYYY');
    }
    if (m < 10){
        m = '0' + m    }
    if (d < 10){
        d = '0' + d    }
    if (format === 'YYYY'){
            return y    }
    else if(format === 'DD MM'){
            return d + delimiter + m    }
    else{
        return d + delimiter + m + delimiter + y    }};

console.log(formatDate(new Date(2021, 10, 22), 'DD MM YYYY', '/')); // 22/10/2021
console.log(formatDate(new Date(2021, 10, 22), 'DD MM', '.')); // 22.10
console.log(formatDate(new Date(2021, 10, 22), 'YYYY', '.')); // 2021
