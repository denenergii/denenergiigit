# Lectrum JS personal prjects


### Installation

```
npm install
```

### Start Dev Server

```
npm start
```

### Build Prod Version

```
npm run build
```

### Pages
1. [index.html](http://127.0.0.1:8181/index.html)
2. [signup.html](http://127.0.0.1:8181/signup.html)
3. [search-car.html](http://127.0.0.1:8181/search-car.html)
4. [search-results.html](http://127.0.0.1:8181/search-results.html)
5. [car-page.html](http://127.0.0.1:8181/car-page.html)
6. [driver-cabinet.html](http://127.0.0.1:8181/driver-cabinet.html)
7. [cabinet.html](http://127.0.0.1:8181/cabinet.html)
8. [checkout.html](http://127.0.0.1:8181/checkout.html)
9. [confirmation.html](http://127.0.0.1:8181/confirmation.html)

### Features:

* ES6 Support via [babel](https://babeljs.io/) (v7)
* SASS Support via [sass-loader](https://github.com/jtangelder/sass-loader)
* Linting via [eslint-loader](https://github.com/MoOx/eslint-loader)