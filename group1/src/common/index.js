// код который будет использоваться для подключения на всех страницах
import 'jquery';
import queryString from "query-string";
import dictionary from "../dictionary.json";

export const getElBySel = (sel) => document.querySelector(sel);
export const getElById = (id) => document.getElementById(id);

export const translate = (key) => {
  const queries = queryString.parse(window.location.search);
  const language = queries.lang ? queries.lang : 'ru';
  return dictionary[language][key];
};
export const translateText = (key) => {
  const el = getElBySel(`.${key}`);
  el.innerText = translate(key);
};
export const changeLang = (url) => {
  const lang = getElBySel('.lang__picker');
  const actualLang = getElBySel('.lang--actual');
  const imgArrow = getElBySel('.arrow');

  actualLang.innerText = translate("lang");
  actualLang.appendChild(imgArrow);

  lang.addEventListener('click', (e) => {
    lang.classList.toggle('open');

    if(!e.target.classList.contains('lang--actual')){
      const selectedLang = e.target.dataset.lang;
      const queries = queryString.parse(window.location.search);
      queries.lang = selectedLang;
      window.location = `${window.location.origin}${url}${queryString.stringify(queries)}`;
    }
  });
};
export const translateDropdown = (key) => {
  const role = getElById('role');
  role.value = translate(key);
};
export const translateValueId = (key) => {
  const el = getElById(key);
  el.value = translate(key);
};
export const translateValueSel = (key) => {
  const el = getElBySel(`.${key}`);
  el.value = translate(key);
};

