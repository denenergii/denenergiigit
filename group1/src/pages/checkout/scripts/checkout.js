import Ajv from 'ajv';
import addFormats from "ajv-formats";
const ajv = new Ajv({ allErrors: true });

addFormats(ajv);

import toastr from 'toastr';
import { v4 as uuidv4 } from 'uuid';
import {localStore} from '../../mocks/localStore';
import {setCheckout} from '../../signUp/scripts/api';

const currency = 'грн';
const queryString = require('query-string');
const parsed = queryString.parse(location.search);  //=> {id: 'some1'}
const orderId = parsed.id ? parsed.id : 'some1';
const orders = localStore.history;
const currOrder = orders.find(item => item.id === orderId);
const cars = localStore.cars;

const tabsetList = document.querySelector('.tab__navigation');
const tabsetLi = tabsetList.querySelectorAll('.tab__navigation li');
const step1holder = document.querySelector('.contact__data');
const step2holder = document.querySelector('.order__data');
const step3holder = document.querySelector('.payment__data');

const areaField = document.getElementById('area');
const streetField = document.getElementById('street');
const messageField = document.getElementById('message');
const cardNumberField = document.getElementById('cardNumber');
const expirationDateField = document.getElementById('expirationDate');
const cvvField = document.getElementById('cvv');
const ownerNameField = document.getElementById('ownerName');
const passwordField = document.getElementById('password');
const submitBtn1 = step1holder.querySelector('button');
const submitBtn2 = step2holder.querySelector('button');
const submitBtn3 = step3holder.querySelector('button');

const carField = step2holder.querySelector('h2[data-info="car"]');
const cityField = step2holder.querySelector('p[data-info="city"]');
const startTimeField = step2holder.querySelector('p[data-info="startTime"]');
const endTimeField = step2holder.querySelector('p[data-info="endTime"]');
const startDayField = step2holder.querySelector('p[data-info="startDay"]');
const endDayField = step2holder.querySelector('p[data-info="endDay"]');
const addressField = step2holder.querySelector('p[data-info="address"]');
const detailsField = step2holder.querySelector('p[data-info="details"]');
const transmissionField = step2holder.querySelector('p[data-info="transmission"]');
const baggageField = step2holder.querySelector('p[data-info="baggage"]');
const climaField = step2holder.querySelector('p[data-info="clima"]');
const babyField = step2holder.querySelector('p[data-info="baby"]');
const amountField = step2holder.querySelector('p[data-info="amount"]');

let checkout = {};

const schema_step1 = {
	$schema: "http://json-schema.org/draft-07/schema#",
	type: 'object',
	additionalProperties: false,
	required: ['area', 'street'],
	properties: {
		area: {
			type: 'string',
			minLength: 5,
		},
		street: {
			type: 'string',
			minLength: 5,
		},
		message: {
			type: 'string',
			maxLength: 255,
		}
	},
};

const schema_step3 = {
	$schema: "http://json-schema.org/draft-07/schema#",
	type: 'object',
	additionalProperties: false,
	required: ['cardNumber', 'expirationDate', 'cvv'],
	properties: {
		cardNumber: {
			type: 'string',
			minLength: 19,
			maxLength: 19,
		},
		expirationDate: {
			type: 'string',
			minLength: 5,
			maxLength: 5,
		},
		cvv: {
			type: 'string',
			minLength: 3,
			maxLength: 3,
		},
		ownerName: {
			type: 'string',
		}
	},
};

submitBtn1.addEventListener('click', function(e) {
	e.preventDefault();

	const validate = ajv.compile(schema_step1);
	const formFields = getFormData_step1();
	const isValid = validate(formFields);

	if(isValid) {
		checkout = {
			id: uuidv4(),
			idOrder: currOrder.id,
			area: formFields.area,
			street: formFields.street,
			message: formFields.message,
		};

		addressField.innerText = `${checkout.area} ${checkout.street}`;
		detailsField.innerText = checkout.message;
		hideShowBlock(step1holder, step2holder);
		tabsetLi[0].classList.remove('active');
		tabsetLi[1].classList.add('active');
	} else {
		if(validate.errors) {
			validate.errors.forEach(function(error) {
				toastr.error(error.message);
			});
		}
	}
});


// step 2

submitBtn2.addEventListener('click', function(e) {
	e.preventDefault();

	tabsetLi[1].classList.remove('active');
	tabsetLi[2].classList.add('active');
	hideShowBlock(step2holder, step3holder);
});

function fillFields() {
	const car = cars.find(item => item.id === currOrder.carId);

	// startTimeField.innerText = currOrder.start;
	// endTimeField.innerText = currOrder.start;
	// startDayField.innerText = currOrder.end;
	// endDayField.innerText = currOrder.end;
	carField.innerText = `${car.vendor} ${car.model}`;
	cityField.innerText = car.city;
	transmissionField.innerText = car.transmission === 'auto' ? transmissionField.getAttribute('data-value-auto') : transmissionField.getAttribute('data-value-manual');
	baggageField.innerText = car.baggage;
	climaField.innerText = car.clima ? climaField.getAttribute('data-value-yes') : climaField.getAttribute('data-value-no');
	babyField.innerText = car.baby ? babyField.getAttribute('data-value-yes') : babyField.getAttribute('data-value-no');
	amountField.innerText = `${currOrder.amount} ${currency}`;
}

fillFields();


// step 3

submitBtn3.addEventListener('click', function(e) {
	e.preventDefault();

	const validate = ajv.compile(schema_step3);
	const formFields = getFormData_step3();
	const isValid = validate(formFields);

	if(isValid) {
		checkout.cardNumber = formFields.cardNumber;
		checkout.expirationDate = formFields.expirationDate;
		checkout.cvv = formFields.cvv;
		checkout.ownerName = formFields.ownerName;

		setCheckout(checkout);
		toastr.success('Order is paid');
	} else {
		if(validate.errors) {
			validate.errors.forEach(function(error) {
				toastr.error(error.message);
			});
		}
	}
});

cardNumberField.addEventListener('keyup', function () {
	formatField(this, 4, ' ');
});

expirationDateField.addEventListener('keyup', function () {
	formatField(this, 2, '/');

	if(this.value.length === 6) {
		const value = this.value;

		this.value = value.substring(0, value.length - 1);
	}
});

cvvField.addEventListener('focus', function () {
	this.type = 'text';
});

cvvField.addEventListener('blur', function () {
	const $this = this;

	if($this.value.length) {
		setTimeout(function() {
			$this.type = 'password';
		}, 1000);
	}
});


// utils
function getFormData_step1() {
	return {
		area: areaField.value,
		street: streetField.value,
		message: messageField.value,
	};
}

function getFormData_step3() {
	return {
		cardNumber: cardNumberField.value.trim(),
		expirationDate: expirationDateField.value.trim(),
		cvv: cvvField.value.trim(),
		ownerName: ownerNameField.value.trim()
	};
}

function hideShowBlock(current, target) {
	current.classList.remove('show');
	target.classList.add('show');
};

function formatField(elem, after, symbol) {
	after = after || 4;

	const val = elem.value.replace(/[^\dA-Z]/g, '');
	const reg = new RegExp(".{" + after + "}","g");

	elem.value = val.replace(reg, function (a, b, c) {
		return a + symbol;
	});
}
