import Ajv from 'ajv';
import addFormats from "ajv-formats";
const ajv = new Ajv({ allErrors: true });

addFormats(ajv);

import {schema, schemaPhone} from './validation';
import toastr from 'toastr';

import {credentials, user, users, car, cars} from './setData';
import { v4 as uuidv4 } from 'uuid';
import {registerCars, registerUsers, getUsers} from '../../signUp/scripts/api';
import {filterItems} from './utils';

const removeCarBtn = document.querySelector('.cancel');
const submitBtn = document.querySelector('input[type="submit"]');

const userNameField = document.getElementById('username');
const userSurnameField = document.getElementById('userSurname');
const phoneNumberField = document.getElementById('phoneNumber');

const city = document.getElementById('city');
const carBrand = document.getElementById('carBrand');
const carBrandModel = document.getElementById('carBrandModel');
const transmission = document.getElementById('transmission');
const pricePerHour = document.getElementById('pricePerHour');
const passangerNumber = document.getElementById('passangerNumber');
const luggageSpace = document.getElementById('luggageSpace');
const govNumber = document.getElementById('govNumber');

const msgRemoveCar = 'Убрать машину из проката';
const msgAddCar = 'Добавить машину в прокат';

let isRegistredCar = Object.keys(car).length;

if(isRegistredCar) {
	setFieldData();
}

if(car.disabled) {
	removeCarBtn.value = msgAddCar;
	removeCarBtn.setAttribute('data-value', 'true');
} else {
	removeCarBtn.value = msgRemoveCar;
	removeCarBtn.setAttribute('data-value', 'false');
}

// phoneNumberField.onblur = function(e) {
// 	const regPhone = /^[0-9()\\-\\.\\s]+$/;
// 	const value = this.value;

// 	if(!value.match(regPhone)) {
// 		toastr.error('The phone is incorrect');
// 		phoneNumberField.value = user.phone;

// 	} else if (value.match(regPhone) && user.phone !== value) {
// 		if(filterItems(credentials.drivers, 'phone', value).length) {
// 			toastr.error('The phone is already registered');
// 			phoneNumberField.value = user.phone;
// 		} else {
// 			user.phone = value;
// 			registerUsers(credentials);

// 			toastr.success('The phone is updated');
// 		}
// 	}
// };

removeCarBtn.addEventListener('click', function(e) {
	e.preventDefault();

	if(this.value === msgRemoveCar) {
		this.value = msgAddCar;
		removeCarBtn.setAttribute('data-value', 'true');

		if(isRegistredCar) {
			car.disabled = true;
			registerCars(cars);

			toastr.success('Your car is removed from the car offer list');
		}
	} else {
		this.value = msgRemoveCar;
		removeCarBtn.setAttribute('data-value', 'false');

		if(isRegistredCar) {
			car.disabled = false;
			registerCars(cars);

			toastr.success('Your car is added to the car offer list');
		}
	}
});

submitBtn.addEventListener('click', function(e) {
	e.preventDefault();

	const validate = ajv.compile(schema); // { object, errors } object == function
	const formFields = getFormData();
	const isValid = validate(formFields);

	if(isValid) {
		user.name = `${formFields.username} ${formFields.userSurname}`;

		if (user.phone !== formFields.phone && filterItems(credentials.drivers, 'phone', formFields.phone).length) {
			toastr.error('The phone is already registered');
			phoneNumberField.value = user.phone;
		} else if(user.phone !== formFields.phone) {
			user.phone = formFields.phone;
			registerUsers(credentials);

			toastr.success('The phone is updated');
		}

		register();
	} else {
		if(validate.errors) {
			validate.errors.forEach(function(error) {
				toastr.error(error.message);
			});
		}
	}

	function register() {
		if(isRegistredCar) {
			car.city = formFields.city;
			car.vendor = formFields.vendor;
			car.model = formFields.model;
			car.transmission = formFields.transmission;
			car.price = formFields.price;
			car.passengers = formFields.passengers;
			car.baggage = formFields.baggage;
			car.num = formFields.num;
			car.clima = getRadioValue('conditioner') === 'true' ? true: false,
			car.baby = getRadioValue('childSeat') === 'true' ? true: false,

			registerCars(cars);
			toastr.success('The car info is updated');
		} else {
			const obj = {
				id: uuidv4(),
				city: formFields.city,
				owner: user.email,
				vendor: formFields.vendor,
				model: formFields.model,
				transmission: formFields.transmission, // manual or auto
				price: parseInt(pricePerHour.value),
				passengers: parseInt(passangerNumber.value),
				baggage: luggageSpace.value,
				num: govNumber.value,
				clima: getRadioValue('conditioner') === 'true' ? true: false,
				baby: getRadioValue('childSeat') === 'true' ? true: false,
				disabled: removeCarBtn.getAttribute('data-value') === 'true' ? true : false
			};
			cars.push(obj);
			registerCars(cars);
			toastr.success('The car is registered');

			isRegistredCar = true;
		}
	}
});

function setFieldData() {
	city.value = car.city;
	carBrand.value = car.vendor;
	carBrandModel.value = car.vendor;
	transmission.name = car.transmission;
	pricePerHour.value = car.price;
	passangerNumber.value = car.passengers;
	luggageSpace.value = car.baggage;
	govNumber.value = car.num;

	resetDropdown(transmission, transmission.name);
	setRadioChecked('conditioner', car.clima);
	setRadioChecked('childSeat', car.baby);
}

function getFormData() {
	return {
		username: userNameField.value,
		userSurname: userSurnameField.value,
		phone: phoneNumberField.value,
		city: city.value,
		vendor: carBrand.value,
		model: carBrandModel.value,
		transmission: transmission.name,
		price: parseInt(pricePerHour.value),
		passengers: parseInt(passangerNumber.value),
		baggage: luggageSpace.value,
		num: govNumber.value,
		clima: getRadioValue('conditioner') === 'true' ? true: false,
		baby: getRadioValue('childSeat') === 'true' ? true: false,
		disabled: removeCarBtn.getAttribute('data-value') === 'true' ? true : false
	};
}

function getRadioValue(name) {
	return document.querySelector(`input[name=${name}]:checked`).getAttribute('data-value');
}

function setRadioChecked(name, value) {
	document.querySelector(`input[name=${name}][data-value=${value}]`).checked = 'true';
}

function resetDropdown(input, value) {
	const text = document.querySelector(`li[data-name=${value}]`).innerText;

	input.value = text;
}


// Из советов - аккуратнее с сохранением по ссылке, при возникновении багов, такой подход ОЧЕНЬ сложно дебажить, что и где изменило изначальный объект

// Для апдейта данных о машине или пользователе, я советую создать функцию апдейта например в случае обновления данных о машине у такой функции будет подобная сигнатура

// const updateCar = (updatedCar) => {
// const updatedCars = getAllCars().map((car) => car.id === updatedCar.id ? updatedCar : car); 

// setAllCars(updatedCars) <= тут мы записываем в наш локалсторадж
// }
// для поика чего-то одного настроятельно советую использовать Array.find()