import {filterItems, utf8_to_b64, b64_to_utf8} from './utils';

import {localStore} from '../../mocks/localStore';
import {getUsers, getCars} from '../../signUp/scripts/api';

export const credentials = getUsers();
export const cars = getCars();
const token = localStorage.getItem('token') ? localStorage.getItem('token') : localStore.token;
const dataToken = b64_to_utf8(token).split(':');
export const [email] = dataToken.filter(item => item.indexOf('@') !== -1);

const userNameField = document.getElementById('username');
const userSurnameField = document.getElementById('userSurname');
const phoneNumberField = document.getElementById('phoneNumber');

export const [user] = getUser();
export const [car] = getCar().length ? getCar() : [{}];

const fullName = user.name.split(' ');
const userName = fullName[0];
const userSurname = fullName[1];

setFormData();

function setFormData() {
	userNameField.value = userName;
	userSurnameField.value = userSurname;
	phoneNumberField.value = user.phone;
}

function getUser() {
	return filterItems(credentials.drivers, 'email', email);
}

function getCar() {
	return filterItems(cars, 'owner', email);
}