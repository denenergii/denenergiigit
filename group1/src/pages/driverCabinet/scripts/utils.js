export const filterItems = (arr, field, query) => {
	return arr.filter(function(item) {
		return item[field] === query;
	});
};

export const utf8_to_b64 = (str) => {
	return window.btoa(unescape(encodeURIComponent(str)));
};

export const b64_to_utf8 = (str) => {
	return decodeURIComponent(escape(window.atob(str)));
};
