export const schema = {
	$schema: "http://json-schema.org/draft-07/schema#",
	type: 'object',
	additionalProperties: false,
	required: ['username', 'userSurname', 'phone', 'city', 'vendor', 'model', 'transmission', 'passengers', 'price', 'baggage', 'num', 'clima', 'baby', 'disabled'],
	properties: {
		username: {
			type: 'string',
			minLength: 3,
			maxLength: 15,
		},
		userSurname: {
			type: 'string',
			minLength: 3,
			maxLength: 15,
		},
		phone: {
			type: 'string',
			pattern: '^[0-9()\\-\\.\\s]+$',
		},
		city: {
			type: 'string',
			minLength: 2,
		},
		vendor: {
			type: 'string',
			minLength: 2,
		},
		model: {
			type: 'string',
			minLength: 2,
		},
		transmission: {
			enum: ['manual', 'auto'],
			description: "Car transmission",
		},
		passengers: {
			type: 'integer',
			minimum: 1
		},
		price: {
			type: 'number',
			minimum: 0,
		},
		baggage: {
			type: 'string',
		},
		num: {
			type: 'string',
			minLength: 4,
		},
		clima: {
			type: 'boolean',
		},
		baby: {
			type: 'boolean',
		},
		disabled: {
			type: 'boolean',
		}
	}
};
