import '../../../css/style.scss';
import {
  changeLang,
  translateDropdown,
  translateText, translateValueSel
} from "../../../common";
import './signUp';
translateText('section__title');
translateText('input-label-1');
translateText('input-select-1');
translateText('input-select-2');
translateText('input-label-2');
translateText('input-label-3');
translateText('registration-link');
translateValueSel('button');

changeLang('?');
translateDropdown('input-select-1');


const dropClass = '.dropdown';
const dropdownItem = '.dropdown__list li';
export const dropdowns = () => {
	document.querySelectorAll(dropClass).forEach(function(dropdown) {
		dropdown.addEventListener('click', function(e) {
			this.classList.toggle('open');
		});
	});

	document.querySelectorAll(dropdownItem).forEach(function(item) {
		item.addEventListener('click', function(e) {
			const holder = this.closest(dropClass);
			const input = holder.querySelector('input');
			const item = this.innerText;
			input.value = item;
			input.name = this.getAttribute('data-name');
		});
	});
};
dropdowns();
let y = JSON.parse(localStorage.getItem("credentials"))
const button = document.querySelector('.button');
const logIn = {
    email: '',
    password: '',
    role: '',
  };
  const inLog = {
    email: '',
    time: '',
    role: '',
  };
button.addEventListener('click', function(e){
	e.preventDefault();
	const role = document.getElementById('role');
	logIn.role = role.value;
	const email = document.getElementById('email');
	logIn.email = email.value;
	const password = document.getElementById('password');
	logIn.password = password.value;
	const b64_to_utf8 = (str) => {
		return decodeURIComponent(escape(window.atob(str)));
	};
//countdown start
let timestamp = 10;
let countdown = setInterval( function(){
timestamp -= 1;
let hours = Math.floor(timestamp / 60 / 60);
let minutes = Math.floor(timestamp / 60) - (hours * 60);
let seconds = timestamp % 60;
let formatted = [
	hours.toString().padStart(2, '0'),
	minutes.toString().padStart(2, '0'),
	seconds.toString().padStart(2, '0')
  ].join(':');
if (timestamp === 0){
	// window.location = 'index.html'
	console.log('yes')
}
inLog.time = formatted;
		const token = JSON.parse(localStorage.getItem('token')) || [];
		localStorage.setItem('token', JSON.stringify([{
		...inLog
		}, ...token]));
		Storage.duration = 10;
return timestamp
}, 1000);
// countdown end
	inLog.email = email.value;
	inLog.role = role.value;
		const token = JSON.parse(localStorage.getItem('token')) || [];
		localStorage.setItem('token', JSON.stringify([{
		...inLog
		}, ...token]));
	let d = y.privates.length - 1;
	if(role.value === 'Арендодатель транспортного средства'){
		for(let i=0; i < y.privates.length; i++){
			const pas = b64_to_utf8(y.privates[i].password);
			if(y.privates[i].email === email.value && +pas === +password.value){
					window.location = 'search-car.html'
			}else if (y.privates[i].email !== email.value){
				alert('Такого аккаунта не существует')
			}else {
				password.style.border = 'solid 2px #f33131'
				email.style.border = 'solid 2px #f33131'
			}
		}
	}else if(role.value === 'Водитель'){
		for(let i=0; i < y.drivers.length; i++){
			console.log(y.drivers.length)
			const pas = b64_to_utf8(y.drivers[i].password);
			if(y.drivers[i].email === email.value && +pas === +password.value){
					window.location = 'cabinet.html'
			}else if (y.drivers[i].email !== email.value){
				alert('Такого аккаунта не существует')
			}else{
				password.style.border = 'solid 2px #f33131'
				email.style.border = 'solid 2px #f33131'
			}
		}
	} 
})
