import Ajv from 'ajv';
import addFormats from "ajv-formats";
const ajv = new Ajv({ allErrors: true });

addFormats(ajv);

import {schema} from './validation';
import toastr from 'toastr';


const kindField = document.getElementById('role');
const emailField = document.getElementById('email');
const passwordField = document.getElementById('password');

function getFormData() {
	return {
		kind: kindField.name,
		email: emailField.value,
		password: passwordField.value
	};
}

function register() {
	const validate = ajv.compile(schema); // { object, errors } object == function
	const formFields = getFormData();
	const isValid = validate(formFields);

	if (!isValid) {
		if(validate.errors) {
			validate.errors.forEach(function(error) {
				toastr.error(error.message);
			});
		}
	}
}
const button = document.querySelector('.button');
button.addEventListener('click', function(e) {

	register();
});