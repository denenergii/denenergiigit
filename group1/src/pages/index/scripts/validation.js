export const schema = {
	$schema: "http://json-schema.org/draft-07/schema#",
	type: 'object',
	additionalProperties: false,
	required: ['kind', 'email', 'password'],
	properties: {
		kind: {
			enum: ['driver', 'private'],
			description: "User's kind",
		},

		email: {
			type: 'string',
			format: 'email',
		},

		password: {
			type: 'string',
			minLength: 8,
		},
	}
};