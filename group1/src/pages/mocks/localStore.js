export const localStore = {
	token: 'ZHJpdmVyOmpkb2UyQGxlY3RydW0uaW86MTYwNzkwMjI1MDYxMg==',
	credentials: {
		drivers: [
			{
				"kind": "driver", // driver or private
				"name": "John Doe",
				"phone": "380975557788",
				"email": "jdoe@lectrum.io",
				"license": "CN 123456",
				"password": "MTIzNDU2Nzg=" // base64 encoded string of 12345678
			},
			{
				"kind": "driver", // driver or private
				"name": "Kile Kerry",
				"phone": "380681111111",
				"email": "test@test.com",
				"license": "CN 123456",
				"password": "MTIzNDU2Nzg=" // base64 encoded string of 12345678
			},
			{
				"kind": "driver", // driver or private
				"name": "Brad Pitt",
				"phone": '380975557788',
				"email": "jdoe3@lectrum.io",
				"license": "CN 123456",
				"password": "MTIzNDU2Nzg=" // base64 encoded string of 12345678
			}
		],
		private: [
			{
				"kind": "private", // driver or private
				"name": "Sara Connor",
				"phone": "380975557788",
				"email": "jdoe@lectrum.io",
				"license": "CN 123456",
				"password": "MTIzNDU2Nzg=" // base64 encoded string of 12345678
			},
			{
				"kind": "private", // driver or private
				"name": "Satti Jones",
				"phone": "380975557788",
				"email": "jdoe2@lectrum.io",
				"license": "CN 123456",
				"password": "MTIzNDU2Nzg=" // base64 encoded string of 12345678
			},
			{
				"kind": "private", // driver or private
				"name": "Mary Ford",
				"phone": "380975557788",
				"email": "jdoe3@lectrum.io",
				"license": "CN 123456",
				"password": "MTIzNDU2Nzg=" // base64 encoded string of 12345678
			}
		],
	},
	cars: [
		{
			"id": "some-car-id1",
			"city": "Харьков",
			"owner": "jdoe2@lectrum.io",
			"vendor": "Nissan",
			"model": "Leaf",
			"transmission": "manual", // manual or auto
			"passengers": 5,
			"price": 20,
			"baggage": "2+1",
			"num": "AA7777AA",
			"clima": false,
			"baby": true,
			"disabled": false
		},
		{
			"id": "some-car-id2",
			"city": "Киев",
			"owner": "jdoe3@lectrum2.io",
			"vendor": "Nissan",
			"model": "Leaf",
			"transmission": "manual", // manual or auto
			"passengers": 5,
			"price": 50,
			"baggage": "2+1",
			"num": "AA7777AA",
			"clima": false,
			"baby": false,
			"disabled": true
		},
		{
			"id": "some-car-id3",
			"city": "Киев",
			"owner": "jdoe3@lectrum2.io",
			"vendor": "Nissan",
			"model": "Leaf",
			"transmission": "manual", // manual or auto
			"passengers": 5,
			"price": 20,
			"baggage": "2+1",
			"num": "AA7777AA",
			"clima": true,
			"baby": false,
			"disabled": true
		},
		{
			"id": "some-car-id4",
			"city": "Киев",
			"owner": "jdoe3@lectrum2.io",
			"vendor": "Nissan",
			"model": "Leaf",
			"transmission": "manual", // manual or auto
			"passengers": 5,
			"price": 20,
			"baggage": "2+1",
			"num": "AA7777AA",
			"clima": true,
			"baby": false,
			"disabled": true
		},
		{
			"id": "some-car-id5",
			"city": "Киев",
			"owner": "jdoe3@lectrum2.io",
			"vendor": "Nissan",
			"model": "Leaf",
			"transmission": "manual", // manual or auto
			"passengers": 5,
			"price": 20,
			"baggage": "2+1",
			"num": "AA7777AA",
			"clima": true,
			"baby": true,
			"disabled": true
		},
		{
			"id": "some-car-id6",
			"city": "Киев",
			"owner": "jdoe3@lectrum2.io",
			"vendor": "Nissan",
			"model": "Leaf",
			"transmission": "manual", // manual or auto
			"passengers": 5,
			"price": 20,
			"baggage": "2+1",
			"num": "AA7777AA",
			"clima": true,
			"baby": false,
			"disabled": true
		},
		{
			"id": "some-car-id7",
			"city": "Киев",
			"owner": "jdoe3@lectrum2.io",
			"vendor": "Nissan",
			"model": "Leaf",
			"transmission": "manual", // manual or auto
			"passengers": 5,
			"price": 20,
			"baggage": "2+1",
			"num": "AA7777AA",
			"clima": true,
			"baby": false,
			"disabled": true
		}
	],
	history: [
		{
			id: "some1",
			email: "jdoe@lectrum.io",
			carId: "some-car-id1",
			start: "start date and time",
			end: "end date and time",
			baby: true,
			amount: 3500
		},
		{
			id: "some2",
			email: "jdoe@lectrum.io",
			carId: "some-car-id2",
			start: "start date and time",
			end: "end date and time",
			baby: true,
			amount: 3500
		},
		{
			id: "some3",
			email: "jdoe@lectrum.io",
			carId: "some-car-id2",
			start: "start date and time",
			end: "end date and time",
			baby: true,
			amount: 3500
		},
		{
			id: "some4",
			email: "jdoe@lectrum.io",
			carId: "some-car-id1",
			start: "start date and time",
			end: "end date and time",
			baby: true,
			amount: 3500
		},
		{
			id: "some5",
			email: "jdoe@lectrum.io",
			carId: "some-car-id1",
			start: "start date and time",
			end: "end date and time",
			baby: true,
			amount: 3500
		},
		{
			id: "some6",
			email: "jdoe@lectrum.io",
			carId: "some-car-id1",
			start: "start date and time",
			end: "end date and time",
			baby: true,
			amount: 3500
		},
		{
			id: "some7",
			email: "jdoe@lectrum.io",
			carId: "some-car-id2",
			start: "start date and time",
			end: "end date and time",
			baby: true,
			amount: 3500
		},
		{
			id: "some8",
			email: "jdoe@lectrum.io",
			carId: "some-car-id3",
			start: "start date and time",
			end: "end date and time",
			baby: true,
			amount: 3500
		},
		{
			id: "some9",
			email: "jdoe@lectrum.io",
			carId: "some-car-id4",
			start: "start date and time",
			end: "end date and time",
			baby: true,
			amount: 3500
		},
		{
			id: "some10",
			email: "jdoe@lectrum.io",
			carId: "some-car-id4",
			start: "start date and time",
			end: "end date and time",
			baby: true,
			amount: 3500
		},
		{
			id: "some11",
			email: "jdoe@lectrum.io",
			carId: "some-car-id4",
			start: "start date and time",
			end: "end date and time",
			baby: true,
			amount: 3500
		},
		{
			id: "some12",
			email: "jdoe@lectrum.io",
			carId: "some-car-id4",
			start: "start date and time",
			end: "end date and time",
			baby: true,
			amount: 3500
		},
		{
			id: "some13",
			email: "jdoe@lectrum.io",
			carId: "some-car-id4",
			start: "start date and time",
			end: "end date and time",
			baby: true,
			amount: 3500
		},
		{
			id: "some14",
			email: "jdoe@lectrum.io",
			carId: "some-car-id5",
			start: "start date and time",
			end: "end date and time",
			baby: true,
			amount: 3500
		},
		{
			id: "some15",
			email: "jdoe@lectrum.io",
			carId: "some-car-id5",
			start: "start date and time",
			end: "end date and time",
			baby: true,
			amount: 3500
		},
		{
			id: "some16",
			email: "jdoe@lectrum.io",
			carId: "some-car-id5",
			start: "start date and time",
			end: "end date and time",
			baby: true,
			amount: 3500
		},
		{
			id: "some17",
			email: "jdoe@lectrum.io",
			carId: "some-car-id5",
			start: "start date and time",
			end: "end date and time",
			baby: true,
			amount: 3500
		},
		{
			id: "some18",
			email: "jdoe@lectrum.io",
			carId: "some-car-id5",
			start: "start date and time",
			end: "end date and time",
			baby: true,
			amount: 3500
		},
		{
			id: "some19",
			email: "jdoe@lectrum.io",
			carId: "some-car-id5",
			start: "start date and time",
			end: "end date and time",
			baby: true,
			amount: 3500
		},
		{
			id: "some20",
			email: "jdoe@lectrum.io",
			carId: "some-car-id6",
			start: "start date and time",
			end: "end date and time",
			baby: true,
			amount: 3500
		},
		{
			id: "some21",
			email: "jdoe@lectrum.io",
			carId: "some-car-id7",
			start: "start date and time",
			end: "end date and time",
			baby: true,
			amount: 3500
		}
	]
};