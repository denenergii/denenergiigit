import Ajv from 'ajv';
import addFormats from "ajv-formats";
const ajv = new Ajv({ allErrors: true });

addFormats(ajv);

import toastr from 'toastr';

import {getFeedbacks, setFeedbacks} from '../../signUp/scripts/api';

const feedbackArray = getFeedbacks();

const usernameField = document.getElementById('username');
const phoneNumberField = document.getElementById('phoneNumber');
const messageField = document.getElementById('message');
const submitBtn = document.querySelector('.user__form input[type="submit"]');

const msg = 'мы свяжемся с вами в течении 15 минут';
const msgNoTime = 'мы свяжемся с вами в 10:00 в ближайший рабочий день';

const schema = {
	$schema: "http://json-schema.org/draft-07/schema#",
	type: 'object',
	additionalProperties: false,
	required: ['username', 'phone', 'message'],
	properties: {
		username: {
			type: 'string',
			minLength: 3,
			maxLength: 15
		},
		phone: {
			type: 'string',
			pattern: '^[0-9()\\-\\.\\s]+$'
		},
		message: {
			type: 'string',
			minLength: 3,
			maxLength: 250
		},
	},
};

submitBtn.addEventListener('click', function(e) {
	e.preventDefault();

	const validate = ajv.compile(schema);
	const formFields = getFormData();
	const isValid = validate(formFields);

	if(isValid) {
		feedbackArray.push({
			username: formFields.username,
			phone: formFields.phone,
			message: formFields.message,
		});

		setFeedbacks(feedbackArray);
		displayMsg();
	} else {
		if(validate.errors) {
			validate.errors.forEach(function(error) {
				toastr.error(error.message);
			});
		}
	}
});

function displayMsg() {
	const now = new Date();

	if(9 < now.getHours() && now.getHours() < 18 && now.getDay() > 0 && now.getDay() < 6) {
		toastr.warning(msg);
	} else {
		toastr.warning(msgNoTime);
	}
}

function getFormData() {
	return {
		username: usernameField.value,
		phone: phoneNumberField.value,
		message: messageField.value,
	};
}