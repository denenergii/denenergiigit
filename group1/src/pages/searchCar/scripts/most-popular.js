import {localStore} from '../../mocks/localStore';

const history = localStore.history ? localStore.history : [];
const cars = localStore.cars;
const holder = document.querySelector('.cars-popular');

const resultArray = history.reduce((array, item) => {
	const elem = array.find(x => x.carId === item.carId);

	if(elem) {
		elem.counter++;
	} else {
		array.push({carId: item.carId, counter: 1 });
	}
	return array;
}, []);

resultArray.sort(function(a, b) { return b.counter - a.counter; });

const renderItem = (car) => {
	const { vendor, model, passengers, baggage, clima, baby, price } = car;

	return `
		<div class="car car__card--vertical">
			<p class="car__type">${vendor}</p>
			<p class="car__brand">${model}</p>

			<img src="img/photo@3x.jpg" class="car__img" alt="">
			<div class="car__params">
				<div class="param">
					<img class="param__icon" src="img/icon/icons-people.svg" alt="">
					<p class="param__data">${passengers}</p>
				</div>
				<div class="param">
					<img class="param__icon" src="img/icon/icons-luggage.svg" alt="">
					<p class="param__data">${baggage}</p>
				</div>
				<div class="param" style="display: ${clima ? 'block' : 'none'}">
					<img class="param__icon" src="img/icon/icons-conditioning.svg" alt="">
				</div>
				<div class="param" style="display: ${baby ? 'block' : 'none'}">
					<img class="param__icon" src="img/icon/icons-child.svg" alt="">
				</div>
			</div>
			<div class="car__price">
				<p class="text">от</p>
				<p class="price">${price} грн</p>
			</div>
		</div>`;
};

const getCarById = (query) => {
	return cars.filter(function(item) {
		return item.id === query;
	});
};

function renderHTML(array) {
	for (let i = 3; i >= 0; i--) {
		const [car] = getCarById(resultArray[i].carId);

		holder.insertAdjacentHTML('afterbegin', renderItem(car));
	};
}

function getItems() {
	if(history.length) {
		renderHTML();
	} else {
		holder.innerText = 'There are no pouplar cars';
	}
}

getItems();