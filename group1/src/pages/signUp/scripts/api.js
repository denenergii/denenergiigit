export const registerUsers = (obj) => {
	localStorage.setItem('credentials', JSON.stringify(obj));
};

export const registerCars = (obj) => {
	localStorage.setItem('cars', JSON.stringify(obj));
};

export const getUsers = () => {
	return localStorage.getItem('credentials') ? JSON.parse(localStorage.getItem('credentials')) : {drivers: [], privates: []};
};

export const getCars = () => {
	return localStorage.getItem('cars') ? JSON.parse(localStorage.getItem('cars')) : [];
};

export const getFeedbacks = () => {
	return localStorage.getItem('feedback') ? JSON.parse(localStorage.getItem('feedback')) : [];
};

export const setFeedbacks = (obj) => {
	localStorage.setItem('feedback', JSON.stringify(obj));
};

export const setCheckout = (obj) => {
	localStorage.setItem('checkout', JSON.stringify(obj));
};
