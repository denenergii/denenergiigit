const dropClass = '.dropdown';
const dropdownItem = '.dropdown__list li';
const dropdown = document.querySelector('.dropdown');
dropdown.addEventListener('click', dropdowns)
export const dropdowns = () => {
	document.querySelectorAll(dropClass).forEach(function(dropdown) {
		dropdown.addEventListener('click', function(e) {
			this.classList.toggle('open');
		});
	});

	document.querySelectorAll(dropdownItem).forEach(function(item) {
		item.addEventListener('click', function(e) {
			const holder = this.closest(dropClass);
			const input = holder.querySelector('input');
			const item = this.innerText;

			input.value = item;
			input.name = this.getAttribute('data-name');
		});
	});
};

dropdowns();

