import '../../../css/style.scss';
import {
  changeLang, translateDropdown, translateText, translateValueId, translateValueSel
} from "../../../common";
import './dropdowns';
import './signUp';

translateText('login');
translateText('section__title');
translateText('input-label-reg');
translateText('input-select-1');
translateText('input-select-2');
translateText('username');
translateText('userSurname');
translateValueId('username');
translateValueId('userSurname');
translateText('input-label-2');
translateText('mobile');
translateText('licence');
translateText('input-label-3');
translateValueSel('reg-button');
changeLang('/signup.html?');
translateDropdown('input-select-1');


const reg_button = document.querySelector('.reg-button');
reg_button.addEventListener('click', function() {
  window.location = 'index.html'
})