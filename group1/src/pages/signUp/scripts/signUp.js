import Ajv from 'ajv';
import addFormats from "ajv-formats";
const ajv = new Ajv({ allErrors: true });

addFormats(ajv);

import {schema} from './validation';
import toastr from 'toastr';
import {registerUsers, getUsers} from './api';
import {filterItems, utf8_to_b64, b64_to_utf8} from '../../driverCabinet/scripts/utils';

const credentials = getUsers();
const kindField = document.getElementById('role');
const userNameField = document.getElementById('username');
const userSurnameField = document.getElementById('userSurname');
const phoneNumberField = document.getElementById('phoneNumber');
const emailField = document.getElementById('email');
const licenseNumberField = document.getElementById('licenseNumber');
const passwordField = document.getElementById('password');
const submitBtn = document.querySelector('input[type="submit"]');

function getFormData() {
	return {
		kind: kindField.name,
		username: userNameField.value,
		userSurname: userSurnameField.value,
		phone: phoneNumberField.value,
		email: emailField.value,
		license: licenseNumberField.value,
		password: passwordField.value
	};
}

function register() {
	const validate = ajv.compile(schema); // { object, errors } object == function
	const formFields = getFormData();
	const isValid = validate(formFields);

	if(registration(formFields)) {
		toastr.error('The user is already registered. Please login in using your credentials');
	} else if (!isValid) {
		if(validate.errors) {
			validate.errors.forEach(function(error) {
				toastr.error(error.message);
			});
		}
	} else {
		const dataObj = {
			kind: formFields.kind,
			name: `${formFields.username} ${formFields.userSurname}`,
			phone: formFields.phone,
			email: formFields.email,
			license: formFields.license,
			password: utf8_to_b64(formFields.password)
		};

		if(dataObj.kind === 'driver') {
			credentials.drivers.push(dataObj);
		} else {
			credentials.privates.push(dataObj);
		}
		registerUsers(credentials);
		window.location.href = 'index.html';
	}

	// console.log(isValid, validate.errors);
}

function registration(obj) {
	if(obj.kind === 'driver') {
		if(filterItems(credentials.drivers,'email',obj.email).length || filterItems(credentials.drivers,'phone',obj.phone).length) {
			return true;
		} else {
			return false;
		}
	} else if (obj.kind === 'private') {
		if(filterItems(credentials.privates,'email',obj.email).length || filterItems(credentials.privates,'phone',obj.phone).length) {
			return true;
		} else {
			return false;
		}
	} else {
		throw new Error(`Invalid field ${obj.kind}. It should be 'driver' or 'private'`);
	}
}

submitBtn.addEventListener('click', function(e) {
	e.preventDefault();

	register();
});

