export const schema = {
	$schema: "http://json-schema.org/draft-07/schema#",
	type: 'object',
	additionalProperties: false,
	required: ['kind', 'username', 'userSurname', 'phone', 'email', 'license', 'password'],
	properties: {
		kind: {
			enum: ['driver', 'private'],
			description: "User's kind",
		},
		username: {
			type: 'string',
			minLength: 3,
			maxLength: 15,
		},
		userSurname: {
			type: 'string',
			minLength: 3,
			maxLength: 15,
		},
		phone: {
			type: 'string',
			pattern: '^[0-9()\\-\\.\\s]+$',
		},
		email: {
			type: 'string',
			format: 'email',
		},
		license: {
			type: 'string',
			minLength: 8,
			maxLength: 8,
		},
		password: {
			type: 'string',
			minLength: 8,
		},
	}
};