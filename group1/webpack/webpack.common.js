const Path = require("path");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const ASSET_PATH = process.env.ASSET_PATH || '';

const entry = {
  index: Path.resolve(__dirname, "../src/pages/index/scripts/index.js"),
  carPage: Path.resolve(__dirname, "../src/pages/carPage/scripts/index.js"),
  checkout: Path.resolve(__dirname, "../src/pages/checkout/scripts/index.js"),
  searchResults: Path.resolve(__dirname, "../src/pages/searchResults/scripts/index.js"),
  confirmation: Path.resolve(__dirname, "../src/pages/confirmation/scripts/index.js"),
  driverCabinet: Path.resolve(__dirname, "../src/pages/driverCabinet/scripts/index.js"),
  searchCar: Path.resolve(__dirname, "../src/pages/searchCar/scripts/index.js"),
  signUp: Path.resolve(__dirname, "../src/pages/signUp/scripts/index.js"),
  cabinet: Path.resolve(__dirname, "../src/pages/cabinet/scripts/index.js"),
};

module.exports = {
  entry: entry,
  output: {
    path: Path.join(__dirname, "../build"),
    filename: "js/[name].js",
    publicPath: ASSET_PATH
  },
  optimization: {
    splitChunks: {
      chunks: "all",
      name: false,
    },
  },
  plugins: [
    new CleanWebpackPlugin(),
    new CopyWebpackPlugin({
      patterns: [{ from: Path.resolve(__dirname, "../public"), to: "." }],
    }),
    new HtmlWebpackPlugin({
      chunks: ["index"],
      filename: "index.html",
      template: Path.resolve(__dirname, "../src/pages/index/index.html"),
    }),
    new HtmlWebpackPlugin({
      chunks: ["carPage"],
      filename: "car-page.html",
      template: Path.resolve(__dirname, "../src/pages/carPage/index.html"),
    }),
    new HtmlWebpackPlugin({
      chunks: ["checkout"],
      filename: "checkout.html",
      template: Path.resolve(__dirname, "../src/pages/checkout/index.html"),
    }),
    new HtmlWebpackPlugin({
      chunks: ["searchResults"],
      filename: "search-results.html",
      template: Path.resolve(__dirname, "../src/pages/searchResults/index.html"),
    }),
    new HtmlWebpackPlugin({
      chunks: ["confirmation"],
      filename: "confirmation.html",
      template: Path.resolve(__dirname, "../src/pages/confirmation/index.html"),
    }),
    new HtmlWebpackPlugin({
      chunks: ["driverCabinet"],
      filename: "driver-cabinet.html",
      template: Path.resolve(__dirname, "../src/pages/driverCabinet/index.html"),
    }),
    new HtmlWebpackPlugin({
      chunks: ["searchCar"],
      filename: "search-car.html",
      template: Path.resolve(__dirname, "../src/pages/searchCar/index.html"),
    }),
    new HtmlWebpackPlugin({
      chunks: ["signUp"],
      filename: "signup.html",
      template: Path.resolve(__dirname, "../src/pages/signUp/index.html"),
    }),
    new HtmlWebpackPlugin({
      chunks: ["cabinet"],
      filename: "cabinet.html",
      template: Path.resolve(__dirname, "../src/pages/cabinet/index.html"),
    }),
  ],
  resolve: {
    alias: {
      "~": Path.resolve(__dirname, "../src"),
    },
  },
  module: {
    rules: [
      {
        test: /\.mjs$/,
        include: /node_modules/,
        type: "javascript/auto",
      },
      {
        test: /\.(ico|jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2)(\?.*)?$/,
        use: {
          loader: "file-loader",
          options: {
            name: "[path][name].[ext]",
          },
        },
      },
    ],
  },
};
