// 1 задание:
const a = 2;
const b = 1;
let result = (a+b > 4) ? true : false;
console.log(result);

// 2 задание:
let login = 'Owner';

let message = (login === 'Pitter') ? 'Hi':
    (login === 'Owner') ? 'Hello':
    (login === '') ? 'unknown': '';
console.log(message);

// 3 задание:
const value = 'c';
if (value === "a"){
    console.log ('a');
}else if (value === 'b' || 'c' || 'd' || 'e'){
    console.log ('others');
}else {
    console.log('unknown');
}

// 4 задание:
const num = 5;
switch (num) {
    case 0:
        console.log (0);
        break;
    case 1:
        console.log (1);
        break;
    case 2:
    case 3:
        console.log ('2, 3');
        break;
    default:
        console.log('unknown');
}